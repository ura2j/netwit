--- 
title: "Moving In"
link title: "Moving In"
type: docs
weight: 10
Description: >
  Find out what rights you have when you are applying for an apartment. This page will tell you how much your landlord can charge for a security deposit, whether your application fee is refundable, and much more.
---

### Table of Contents

1. [Lease Agreement Information](#virginia-lease-agreement-information)
2. [Security Deposits](#virginia-security-deposit-information) 


## Virginia Lease Agreement Information 

Most landlords will make you pay a security deposit before you move in. Under the VRLTA the deposit cannot be more than two months rent. The deposit is held by the landlord until you move out to cover the cost of any damages you may make to the apartment while you live there or any outstanding rent or other charges that you owe. If you leave owing no money and the premises are clean and in generally the same condition as when you first moved in, this deposit will be returned to you. However, if there are damages or money owed, the landlord will keep the Security Deposit. 	

If you are covered by the VRLTA, the landlord may be able to withhold a reasonable portion of the security deposit to cover any unpaid water, sewer, or other utilities that were your responsibility to pay directly to the utility company under the lease agreement. The landlord must first give you notice of the intent to withhold that amount. That notice may be given to you in a termination or vacating notice, or in a separate written notice at least 15 days prior to the landlord’s disposition of the security deposit. If the landlord actually pays the utility bills that were your responsibility, then the landlord must give you written confirmation of that fact within 10 days after the payment was made, along with payment to you of any balance of the security deposit owing. On the other hand, if you can provide written proof to the landlord that you actually paid the utility bills, then the landlord must properly refund the security deposit. 	

If you are covered by the VRLTA, the landlord must return the deposit or send you an itemized list of the damages or charges he or she is deducting from the deposit within 45 days of when you move out. Also under the VRLTA, if you have lived there for more than 13 months, the landlord must give you interest on the deposit as well. 

If you are not covered under the VRLTA, there is no interest or specific time limit for the return of the deposit. However, if it is not returned after a reasonable amount of time, you can go to court and sue for its return. 

### IMPORTANT TIPS:		

 - Always thoroughly check the rooms, appliances, and plumbing before you move into an apartment.
 						
 - As soon as you move in, make a list of all the things wrong with the apartment or house, give a copy to the landlord and keep a copy for yourself. You may also wish to take pictures of the apartment before you move in, so you have a record of the condition of the unit when you moved in. This will protect you from being charged for existing damages later.
 						
 - When you are ready to move out, make an appointment with the landlord to inspect the premises together so you can agree on its condition.
 						
 - If you are concerned as to return of the deposit, you may also want to take pictures when you move out so you can later prove, if need be, how you left the premises.
 						
 - Always return the keys and if you expect return of the deposit, leave a forwarding address.
 						
 - When you move, take everything with you in as short a period of time as possible. Property you leave can be treated as abandoned.
 						
 - Finally, unless specifically agreed to by the landlord, do not use the security deposit to pay your last month’s rent as your landlord could bring an eviction action when the rent is not paid timely.

## Virginia Security Deposit Information  

Most landlords will make you pay a security deposit before you move in. By law the deposit cannot be more than two months' rent. The deposit is held by the landlord until you move out, in case there are any damages to the home, unpaid rent, or other charges you owe. If you didn't damage the home or owe any money, you will get your security deposit back. Otherwise, the landlord gets to keep the security deposit.

When you move out, the landlord can use part of the security deposit to cover any unpaid utility bills you are supposed to pay under the lease. But the landlord must first give you written notice at least 15 days before using the deposit. After paying the utility bills, the landlord must give you written confirmation of payment within 10 days, and also give you the rest of the security deposit. On the other hand, if you can provide written proof that you actually paid the utility bills, the landlord must refund the security deposit.	

The landlord must return the deposit or send you an itemized list of the damages or charges deducted from the deposit within 45 days of when you move out. Also if you have lived there for more than 13 months, the landlord must give you interest on the deposit.

If you are not covered under the VRLTA (See “Legal Information” tab for more information), there is no interest or specific time limit for the return of the deposit. However, if it is not returned after a reasonable amount of time, you can go to court and sue for its return.

### IMPORTANT TIPS:			

 - Always thoroughly check the rooms, appliances, and plumbing before you move in.
 						
 - As soon as you move in, make a list of all the things wrong, give a copy to the landlord and keep a copy for yourself. You may also wish to take pictures before you move in, so you have a record of the condition when you moved in. This will protect you from being charged for existing damages.
 						
 - When you are ready to move out, make an appointment with the landlord to inspect the premises together so you can agree on its condition.
 						
 - If you are concerned about getting the deposit back, you may also want to take pictures when you move out so you can later prove how you left the premises.
 						
 - Always return the keys and if you expect return of the deposit, leave a forwarding address.
 						
 - When you move, make sure to move everything out as quickly as possible. Things you leave behind for too long can be treated as abandoned.
 						
 - Unless specifically agreed to by the landlord, do not assume the security deposit covers your last month’s rent because your landlord could evict you if you don’t pay the rent on time. 		


## [Currently Renting Tenant Information](https://ura2j.gitlab.io/netwit/tenant/rights/currently-renting/)
## [Fill Out and File a Tenant's Assertion](https://ura2j.gitlab.io/netwit/tenant/docs/)




