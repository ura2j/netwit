--- 
title: "Ending Your Lease"
linkTitle: "Ending Your Lease"
type: docs
toc_hide: true
weight: 50
--- 

## When Can I End My Lease? 

Whether your lease is written or just an oral agreement, there are certain procedures both you and your landlord must follow to properly end your tenancy.	

### What Procedures Do I Need to Follow to End My Lease? 

#### I Have a Written Lease 

If you have a written lease, the notice requirements for termination should be contained in the lease. If it is a month to month lease, 30 days is usually required. If it is a year’s lease, the lease will usually state that you must give notice that you will not be renewing the lease 30 or 60 days before the lease ends. Often times, a year’s lease will change into a month to month lease after the year runs or it may renew automatically for another year. **Check the lease carefully.**

#### I Do Not Have a Written Lease 

If you have no written lease and you pay rent by the month, the tenancy can be terminated by either you or the landlord for any reason or no reason at all, by giving at least 30 days written notice before the next rental due date. If you pay rent on a weekly basis, then it would be seven days notice.		

### What If I Moved Out Before My Lease Was Up? 

Remember, if you move out or are evicted before the lease term is up, then you can be held responsible for rent until the lease term expires or the unit is re-rented. For instance, if your lease runs through September and you are evicted or vacate the apartment in May without the landlord’s consent, you will still owe for June, July, August, and September. However, if the landlord re-rents the property, you would no longer owe rent for the months after the apartment is re-rented. A landlord cannot collect rent twice for the same property.

### What If My Landlord Sells the Property? 

If the landlord sells the property, the tenant (lessee) has the same rights against the new owner (grantee) as s/he had against the original owner (grantor). However, the lease would be controlling. Read the lease carefully to see if it says anything about the tenant’s rights after the landlord sells the property. If the lease says nothing about a sale of the property, then the VRLTA applies and the tenant has all the rights usually granted by the law and the lease.		

### My Rental Has Been Forclosed On

There are special rules on notice of termination when the property you’re renting has been foreclosed on and there’s a new owner. If you have a lease and there are more than 90 days remaining on the lease, then you can’t be evicted until the end of the lease. However, if the new owner intends to use the property as his/her primary residence, then you can be given 90 days notice of termination. Also, you must be given 90 days notice if there is no lease, or if there is a lease with fewer than 90 days remaining, or if you have a month-to-month lease.		

There is also a rule that requires your landlord to give you notice if the property may be foreclosed on, even if foreclosure hasn’t occurred yet. It may happen that, even though you and the other tenants are paying your rent to the landlord, the landlord is not current in paying the mortgage on the property. The landlord is required to give you written notice of any of the following: the mortgage is in default, the landlord has received a notice of acceleration of the mortgage (that is, the lender is declaring the whole balance of the mortgage due now), or there’s an upcoming sale of the property upon foreclosure. The landlord must give you this notice within five business days after the landlord has received notice from the lender. If the landlord fails to give you this notice, you may terminate the lease by giving five days written notice to the landlord.	
