---
title: "Tenant Rights"
linkTitle: "Tenant Rights"
type: docs
Menu:
  Main:
    weight: 20
Description: >
  Select an area below to find out more information
weight: 10
---



<details>

<summary>How do I get my landlord to fix something in my home that is broken because it threatens my health or safety? </summary> 

If there is something wrong with your apartment and your landlord has refused to fix it, you can go to your General District Court and file a **_Tenant’s Assertion_**. Once you do this whole process a judge can do one of the following options:

 - make your landlord fix the problem,

 - reduce your rent payments, 

 - hold onto your rent until the problem is fixed, OR

 - terminate your lease so that you can move out. 

Please go to the [Forms and Filing](https://ura2j.gitlab.io/netwit/tenant/docs/) Section To Learn More About this Process.

</details>
