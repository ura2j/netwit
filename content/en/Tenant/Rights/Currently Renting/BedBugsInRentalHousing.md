--- 
title: "Bed Bugs in Rental Housing"
linkTitle: "Bed Bugs in Rental Housing"
type: docs
toc_hide: true
weight: 50
--- 

## What Do I do when there are bed bugs in my rental housing? 

### What does Virginia law say about bed bugs in rental housing?
 
Virginia law does not specifically mention bed bugs.  The law does not explicitly say whether landlords or tenants are responsible for getting rid of bed bugs.  Virginia law does require all landlords to follow building and housing codes affecting health and safety, and requires landlords to make all repairs needed to keep the place fit and habitable.  Because bed bugs make rental housing not habitable, this generally means landlords are responsible for getting rid of bed bugs.
 
### What duties do landlords have about bed bugs?
 
**Under Code of Virginia §§55-248.13 and 55-225.3, a landlord must do two things:**
- Comply with applicable building and housing codes affecting health and safety.
- Make all repairs and do whatever is necessary to put and keep the premises in a fit and habitable condition.
 
### What duties do tenants have about bed bugs?
 
**Under Code of Virginia §§55-248.16 and 55-225.4 a tenant must do three things:**
 
- Comply with all obligations primarily imposed upon tenants by applicable provisions of building and housing codes affecting health and safety.
- Keep that part of the premises the tenant occupies free from insects and pests.
- Promptly notify the landlord of the existence of any insects or pests.
 
### Does this mean the tenant is responsible for getting rid of bed bugs?
 
**No.**  Under Virginia law – Code of Virginia §§55-248.16(A)(14) and 55-225.4(A)(14):

“the tenant shall be financially responsible for the added cost of treatment or extermination due to the tenant’s unreasonable delay in reporting the existence of any insects or pests and be financially responsible for the cost of treatment or extermination due to the tenant’s fault in failing to prevent infestation of any insects or pests in the area occupied.”
 
### What does “tenant’s fault” mean?
 
Under Virginia law, “fault” means the omission of some duty which the tenant ought to
perform, and is almost the same thing as negligence.
 
#### Does this new law apply to all tenants?
 
**Yes.**  Since July 1, 2018, this new law applies to all tenants.  You are a tenant if you pay regular amounts of rent during regular time periods, such as once a month or once a week.  You also are a tenant if you have lived in a hotel or motel for more than 90 days, or you are subject to a written lease for a period of more than 90 days.  You are not a tenant if you have lived in a hotel or motel for less than 90 days.    
 
### A tenant is responsible for cost of treatment only in two situations:
 
- The added cost of treatment or extermination due to the tenant’s unreasonable delay in
   reporting the existence of any insects or pests.
 
- The cost of treatment or extermination due to the tenant’s fault in failing to prevent an infestation of any insects or pests.
 
### So may a landlord bill a tenant for the cost of bed bug treatment?
 
Many landlords do not understand the law and believe they may bill tenants for the cost of bed bug treatment in all situations.  However, other than the two situations listed above, there is no legal authority that allows a landlord to require a tenant to pay for the cost of bed bug treatment.  Anything in the lease that says otherwise cannot be enforced by the landlord against the tenant.
 
### What should a tenant do if billed for the cost of bed bug treatment?
 
If a landlord does bill a tenant for the cost of bed bug treatment, the tenant should not ignore the bill.  The tenant should write the landlord a reply letter.  The letter should say the tenant denies owing the bill because the tenant has no responsibility to pay for the cost of bed bug treatment.  The letter also should say that none of the future payments (rent or otherwise) to the landlord may be applied to the bill for the cost of bed bug treatment
 
In addition, when the tenant makes future payments (rent or otherwise) to the landlord, the tenant should write a letter with each payment.  The letter should say exactly how the payment is to be used (for example, for July rent).  The letter also should say that none of the payment may be applied to the bill for the cost of bed bug treatment.  Finally, if the landlord files a lawsuit against the tenant because the tenant did not pay for the cost of bed bug treatment, the tenant should get legal help right away.
