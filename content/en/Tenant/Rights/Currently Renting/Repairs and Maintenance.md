--- 
title: "Repairs and Maintenance"
linkTitle: "Repairs and Maintenance"
type: docs
toc_hide: true
weight: 30
--- 

## VA Law on Tenant and Landlord Maintenance Requirements 

### Under Virginia law, unless properly agreed otherwise, all landlords must do these things:
 			 					
- Follow building and housing codes affecting health and safety.
 						
- Make all repairs needed to keep the place fit and habitable.
 						
- Keep in good and safe working order all electrical, plumbing, sanitary, heating, ventilating, air conditioning and other facilities and appliances that the landlord supplies.
 						
- Prevent or remove rodent infestations.
 						
- Landlords covered by the VRLTA must also keep clean and safe any common areas used by more than one tenant. 

### What should I do I to do to get my landlord to fix an issue? 

If something needs to be repaired that is the landlord’s responsibility, **you must notify the landlord in writing of the problem and give him or her a reasonable time to fix it.** 

#### When does my landlord have to fix the issue? 

If it is an emergency, such as a lack of heat or water, your landlord should fix it immediately. This means within hours, or at most a day or two. Other repairs must be made within a _reasonable time._  This usually means 1-2 weeks. 

#### What should I include in the letter? 

Your letter should specify the repairs needed and a time by which to fix each problem. As you must give your landlord access to your home to make repairs, you may also want to put in the letter what times of day are best for you or how the landlord can reach you for permission to enter the premises. 

## What If My Landlord Won't Fix the Issue? 

If repairs aren’t made in a reasonable time, you can take your landlord to court with what is called a _"rent escrow" case._ 

Note: 
At this point, it probably is best to get legal help.

#### What Steps Should I to start the rent escrow process? 
To use this procedure, you pay your full rent into court within five days of the date the rent first comes due. You fill out a "Tenant’s Assertion and Complaint" form at the General District Court for the county or city where you live. You can attach a copy of the inspection report or your repair letter to the landlord.

You also can list the bad conditions on the form. 

#### How Much Will It Cost To File the Form? 

To file and serve the papers will cost about $30. You may ask the clerk for a "waiver of fees" if you can’t afford to pay. 

#### What should I include on the Tenants Assertion Form? 

When you fill out the Tenant’s Assertion, you need to decide what you want the judge to do. You can ask the judge for any of these things: 
- to order repairs completed before your rent is released to the landlord
- to order repairs and return of some (or all) of the rent money to you for having to put up with the bad conditions
- to order your lease ended so you can move out without paying future rent.	

#### What Happens Once I File a Tenants Assertion?

After filing the Tenant’s Assertion, the court sets a hearing date and has the landlord served with a "summons to appear in court." You can also ask the clerk to subpoena the building inspector if there was one, and any other witnesses who have agreed to help you. 

##### How much Do Summons Cost? 
Subpoenas cost $12 each unless your filing fees were waived. 

### How should I prepare for the hearing? 

Before the hearing date, you should get your list of problems, a copy of your lease (if written), a copy of your notice letter, your certified mail return receipt, the inspector’s report, any pictures or videos, and your rent receipts. 

#### What Will Happen When The Judge Calls My Case? 

** Note: If you do not come to court on your trial date, the court will dismiss your case.** However, if you come to court and the other side does not, you should get a favorable judgment. If both sides come to court, the judge will hear both sides and decide who wins.  	

When the case is heard, you will present your evidence first. The landlord or judge may ask you questions. Ask the inspector and your witnesses to testify after you. 

Then the landlord gets to present evidence and witnesses. You can question them about what they have said, **but don't argue with them.**  


