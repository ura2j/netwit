--- 
title: "Legal Information"
linkTitle: "Legal Info"
type: docs
weight: 40
Description: >
  Information on what laws and regulations control Virginia housing rights.
--- 

### Statutes 

**The Virginia Residential Landlord and Tenant Act (“VRLTA”), Sections 55.1-1200 through 55.1-1262 of the Code of Virginia, provides the rights and responsibilities of landlords and tenants in Virginia.** 

Generally speaking, the VRLTA, applies to all residential tenancies (leased residential premises) unless the landlord is eligible to opt out, and states this in a written lease. The VRLTA also applies to stays in hotels, motels, or boarding houses if the tenant has been renting for more than 90 days or has a written lease for more than 90 days. Even if your rental is not covered by the VRLTA, there may be other state laws that apply to your situation. If you do not know which law applies, you should seek advice from an attorney.
