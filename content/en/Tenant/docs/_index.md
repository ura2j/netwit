---
title: "Information and Forms for Filing With the Court"
linkTitle: "Forms and Filing"
weight: 20
type: docs
menu:
  main:
    weight: 30
Description: >
  This page contains all forms and information needed to file a Tenant's Assertion in your local General District Court. 
---
 <details>

<summary>What is a Tenant's Assertion?</summary> 
  
  
  A Tenants's Assertion is a legal form for tenants who want to make their landlords repair defects and damages to their homes. Under Virginia law, the first step is sending written notice to the landlord describing the defects. If the letter is unsuccessful, the next step is filing a Tenants Assertion and Complaint in General District Court.
  
  This page contains all infomration needed to file a Tenant's Assertion form in court. 
  
  For information on your righst as a tenant please visit our [tenant's rights](https://ura2j.gitlab.io/netwit/tenant/rights/) page.
  
  </details>
  
---
<details>

  <summary>Tools</summary> 

[Write Your Landlord a Letter and Fill Out A Form](https://lawhelpinteractive.org/Interview/GenerateInterview/7163/engine) 

[Videos](https://ura2j.gitlab.io/netwit/tenant/legal-help/tools/videos/) 

[Speak to the Community](https://ura2j.gitlab.io/netwit/tenant/community/blog/)

</details>

--- 

Select a section below to find out more information 
