---
title: "Add content, Edit this site, or create your own page to link to this page"
linkTitle: "Edit, Add, Build"
type: docs
Description: >
  Select an area below to find out more information
weight: 10
---

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>




<head>
<style>
html {  scroll-behavior: smooth;}
#section1 {}
#section2 {}
</style>
</head>









<div class="main" >

<div class="container">
  <h2>Learn how to add, edit, or build upon the site here</h2>


   <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#Add">Add</a></li>
  <li><a data-toggle="tab" href="#Edit">Edit</a></li>
  <li><a data-toggle="tab" href="#Build">Build your own</a></li> 
  </ul>


  <div class="tab-content">
   <div id="Add" class="tab-pane fade in active">
      <h3>I want to add content</h3>
        <p><a href="https://www.docsy.dev/docs/adding-content/content/">Tutorial of how to add content</a></p>
	      <p></p>
        <p><a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet">Tutorial of how to use Markdown. It is easy, just like Wikipedia</a></p>
   </div>


  <div id="Edit" class="tab-pane fade">
      <h3>I want to edit existing content</h3>
      <p><a href="https://guides.github.com/features/mastering-markdown">Click for a link to a Markdown tutorial</a></p>
	      <p></p>
        <p><a href="https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf">Click for a Markdown PDF cheatsheet</a></p>
  </div>

  <div id="Build" class="tab-pane fade">
      <h3>I want to build a new page</h3>
      <p>Coming soon!</p>
Step 1) <a href="https://gitlab.com/">Create a Gitlab account here</a>

Step 2) <a href="https://www.tutorialspoint.com/gitlab/gitlab_fork_project.htm"> Use this tutorial to fork (basically copy) our site</a>

Step 3) Add and edit content using the instuctions on this page so that you can customize your project to your legal area of expertise.

Step 4) Send us your code through a merge request, email, blog post, or any of the many other means available thoughout our site. We will then add the content after reviewing it and add to the continously developing page. 
  </div>

  </div>
</div>
</div>




</div>
