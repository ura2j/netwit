---
title: "Legal Aid"
linkTitle: "Legal Aid"
type: docs
Description: >
  A collection of Virginia legal aid resources and organizations 
---

# Legal Aid Organizations 

### [Virginia Poverty Law Center](https://vplc.org/)
Virginia Poverty Law Center (VPLC) is a 501(c)3 nonprofit organization committed to breaking down systemic barriers that keep low-income Virginians in the cycle of poverty through advocacy, education, and litigation.

### [VA Legal Aid](https://www.valegalaid.org/)
VaLegalAid.org is a Virginia Partnership for Equal Justice Website Project.

### [Virginia Legal Aid Society](http://vlas.org/about-vlas/)
Virginia Legal Aid Society (VLAS) is a nonprofit law firm established in 1977 to provide free civil legal services to eligible low-income residents in 20 counties and six cities in Central, Southside, and Western Tidewater Virginia.

### [Blue Ridge Legal Services Inc.](www.brls.org)
Blue Ridge Legal Services logoFounded in 1977, its staff of 12 attorneys provides free legal assistance in civil matters of critical importance to low-income residents of the Shenandoah Valley and Roanoke Valley. Offices are in:

### [Central Virginia Legal Aid Society, Inc.](www.cvlas.org)
Central Virginia Legal Aid Society logoFounded in 1974, its staff of 27, 10 of them attorneys, provides legal advice, brief service, negotiation, litigation and representation in administrative hearings. It also provides information and advice through clinics and community education, and partners with local, state and national groups that support legal aid and organizations that provide service to its clients. Offices are in:

### [Legal Aid Justice Center](www.justice4all.org)
Legal Aid Justice Center logoFounded in 1967, its staff of 25 attorneys and seven paralegals and community organizers provide civil legal services throughout Central Virginia. Services assist low-income families, at-risk children, low-wage immigrant workers, institutionalized people, and other vulnerable populations statewide. In its quest to seek equal justice for all, the center also seeks to root out inequities and exploitation that keep people in poverty. The Legal Aid Justice Center receives no funding from the federal Legal Services Corporation. Offices are in:

### [The Legal Aid Society of Eastern Virginia Inc.](http://sites.lawhelp.org/Program/1647/)
Legal Aid Society of Eastern Virginia logoFounded in 1966, its 21 attorneys and four paralegals promote the equal application of justice and work to remove impediments to fairness for low-income and vulnerable families of Eastern Virginia. Offices are in:

### [Legal Aid Society of Roanoke Valley](www.lasrv.org/)
Legal Aid Society of Roanoke Valley 50th logoFounded in 1966, this is Virginia's oldest staff attorney model legal aid society. Its four Roanoke-based attorneys employ legal services to identify and resolve the most critical civil injustices facing low-income people in an eight-county area surrounding Roanoke. The organization routinely lobbies legislative and administrative bodies on behalf of its clients. Funding comes from multiple sources.

Its Domestic Violence Prevention Program provides victims with help getting protective orders, custody, child and spousal support, divorce, housing and other assistance essential to escaping abuse. The program is financed through a state grant from the Virginia Domestic Violence Victims Fund.

# Legal Aid Resources 

### [Virginia - ABA Free Legal Answers](https://virginia.freelegalanswers.org/)
ABA Free Legal Answers is a virtual legal advice clinic for qualifying users to post civil legal questions at no cost. 

### [Find Your Closest Legal Aid Program](https://www.vba.org/page/pro_bono_legalaid)


