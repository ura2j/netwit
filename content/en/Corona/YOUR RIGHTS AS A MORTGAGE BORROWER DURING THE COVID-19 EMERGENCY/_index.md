--- 
title: Your Rights As a Mortgage Borrower During the COVID-19 Emergency
link title: Your Rights As a Mortgage Borrower During the COVID-19 Emergency
type: docs
weight: 40
--- 




## YOUR RIGHTS AS A MORTGAGE BORROWER DURING THE COVID-19 EMERGENCY

(Current as of March 30, 2020)

**Depending on the type of mortgage, your lender may not be allowed to foreclose on you.**

 - Most but not all mortgages are covered by the foreclosure moratorium that Congress passed on March 27, 2020. The moratorium runs from March 18, 2020 until May 17, 2020 and may be extended. The moratorium covers both judicial foreclosures and foreclosures conducted by auction.


 - The moratorium covers Borrowers with "federally backed mortgage loans" and tenants living in a property with such a loan. A "federally backed mortgage loan" is a loan owned, insured or guaranteed by one of the following entities: the Department of Housing and Urban Development (HUD), the Department of Veterans Affairs, the Department of Agriculture, Fannie Mae or Freddie Mac.

       

 - More information on how to find out if your mortgage is covered by the moratorium is available at https://www.nhlp.org/wp-content/uploads/foreclosure-protection-CARES- Act.pdf

**The main source of your rights is your Deed of Trust.**

 - The most important document in your mortgage is your Deed of Trust. That tells you what both your and your lender’s rights and obligations are regarding your mortgage.

 - If you do not have a copy of your Deed of Trust, you can obtain it at your county’s Circuit Court. Contact them first to see how you can access public records during the emergency. You can look up their phone number online at http://www.courts.state.va.us/courts/circuit.html.

**Your lender must comply with the Deed of Trust and applicable law to foreclose.**

 - Before they can foreclose, your lender must give you at least 14 days’ notice of the time, place, and date of the foreclosure sale.

 - They must also advertise the sale in a generally circulated newspaper in your county. This can be at least once a week for two weeks or once a day for three days, depending on your Deed of Trust. If it doesn’t say anything about advertisements, it can be at least once a week for four weeks or once a day for five days.

 - Any other requirements will be in your Deed of Trust. Read it carefully.

 - The Virginia Supreme Court ordered all district and circuit courts in Virginia to postpone all non-essential, non-emergency cases until April 26, 2020. However, Virginia is a non-

judicial foreclosure state. This means that lenders do not have to take you to court, and can foreclose on you through public auction. If your mortgage is not a “federally backed mortgage” the foreclosure moratorium does not apply either.

**Apply for mortgage relief with your servicer if you cannot make your payments.**

 - Unless your lender or your servicer have stated otherwise to you, the Covid-19 emergency does not change your obligation to pay your mortgage.

    - If you are facing financial hardship because of the Covid-19 emergency, apply for mortgage assistance with your servicer. Be sure to provide all the required information stated in the application.

    - If you are faced with foreclosure and you submit a complete mortgage assistance application 37 days or more before the sale date, your servicer must make a decision on your application before they can continue with the foreclosure process. This counts if there is no sale date yet.

    - Keep an accurate written record of all contact you have with your servicer – what they tell you, what you tell them, and when. Keeping a paper trail will help protect your rights.

    - If you have a “federally backed mortgage,” any mortgage servicer that receives a request for payment assistance from a borrower who affirms they are experiencing a financial hardship caused, directly or indirectly, by the COVID-19 emergency, must offer the borrower forbearance of up to 180 days, meaning that monthly payments can be reduced or deferred for up to six months. The servicer must also extend the forbearance for up to an additional 180 days upon request by a borrower during the COVID-19 emergency.

    - A forbearance does not mean that the deferred payments are being forgiven or waived. Once the forbearance ends, the borrower will still have to work with the mortgage servicer to bring the loan current through one of a number of methods, including reinstatement by lump sum, repayment plan or loan modification.


    - You may also wish to consult with a housing counselor if you have further questions or concerns regarding mortgages and foreclosures – for more information, see https://www.hud.gov/i_want_to/talk_to_a_housing_counselor.--